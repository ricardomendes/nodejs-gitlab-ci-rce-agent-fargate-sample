# Node.js sample for GitLab CI and rce-agent on AWS Fargate

Sample Node.js repository demonstrating how to run GitLab CI pipelines with
[github.com/square/rce-agent](https://github.com/square/rce-agent) on AWS
Fargate.

[![pipeline
status](https://gitlab.com/ricardomendes/nodejs-gitlab-ci-rce-agent-fargate-sample/badges/master/pipeline.svg)](https://gitlab.com/ricardomendes/nodejs-gitlab-ci-rce-agent-fargate-sample/-/commits/master)
